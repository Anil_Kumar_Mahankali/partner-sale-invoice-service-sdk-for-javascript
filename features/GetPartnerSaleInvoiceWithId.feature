Feature: Get Partner Sale Invoice With Id
  Gets the partner sale invoice with the provided id

  Scenario: Success
    Given I provide the partnerSaleInvoiceId of a partner sale invoice in the partner-sale-invoice-service
    And provide a valid accessToken identifying me as a partner rep
    When I execute getPartnerSaleInvoiceWithId
    Then the partner sale invoice is returned